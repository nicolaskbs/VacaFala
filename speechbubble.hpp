#ifndef SPEECHBUBBLE_HPP_
#define SPEECHBUBBLE_HPP_

#include <iostream>
#include <vector>
#include <string>

// imprime balão de fala
class SpeechBubble{
private:
    std::vector<std::string> phrase;
    std::vector<std::vector<std::string>> word;
    std::vector<std::string> bubbleLine;
    int wrap = 40;
    bool dontWrap = false;
    std::string alignment = "l";
public:
    void getUserText();
    void getWords();
    void setWrap(std::string ag);
    void setDontWrap();
    void setAlignment(std::string c);
    void drawSpeechBubble();
    void setSpeechBubble();
    int findBiggest();
};

#endif