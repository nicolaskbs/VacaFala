#include "blackbox.hpp"

void BlackBox::printBasicInterface(){
    std::cout << "Welcome to C++ cowsay clone, version by Nicolas Kevin, 2023, DIMAp, UFRN." << std::endl;
    std::cout << "Usage: cowsay [-h] [-bdgpstwy] [-E eyes] [-T tongue] [-F cowfile] [-J alignment] [-W wrapcolumn] [-n] [message]" << std::endl;
    std::cout << "       where - eyes       is a 2-character string. Ex.: '@@'." << std::endl;
    std::cout << "             - tongue     is a 2-character string. Ex.: ' U'." << std::endl;
    std::cout << "             - cowfile    is a text file that contains a cow description." << std::endl;
    std::cout << "             - alignment  `c` means center aligned, `r` means right aligned, `j` means justified." << std::endl;
    std::cout << "             - wrapcolunm is an integer value in [1,100]." << std::endl;
}