#include "blackbox.hpp"
#include "cow.hpp"
#include "speechbubble.hpp"

int main(int argc, char* argv[]){
    std::string argument[argc];
    for(int i = 1; i < argc; i++){
        argument[i] = argv[i];
    }
    Cow cow = Cow(CowType::DEFAULT);
    BlackBox blackBox;
    SpeechBubble speechBubble;
    if(argc == 1){
        speechBubble.drawSpeechBubble();
        cow.drawCow();
        return 0;
    }
    for(int i = 1; i < argc; i++){
        if(argument[i] == "-h"){
            blackBox.printBasicInterface();
            return 0;
        } else if(argument[i] == "-b"){
            cow.setType(CowType::BORG);
        } else if(argument[i] == "-d"){
            cow.setType(CowType::DEAD);
        } else if(argument[i] == "-g"){
            cow.setType(CowType::GREEDY);
        } else if(argument[i] == "-p"){
            cow.setType(CowType::PARANOIA);
        } else if(argument[i] == "-s"){
            cow.setType(CowType::STONED);
        } else if(argument[i] == "-t"){
            cow.setType(CowType::TIRED);
        } else if(argument[i] == "-w"){
            cow.setType(CowType::WIRED);
        } else if(argument[i] == "-y"){
            cow.setType(CowType::YOUNG);
        } else if(argument[i] == "-E"){
            cow.userEyes(argument[i+1]);
            i++;
        } else if(argument[i] == "-T"){
            cow.userTongue(argument[i+1]);
            i++;
        } else if(argument[i] == "-f"){
            cow.setFlip();
        } else if(argument[i] == "-W"){
            speechBubble.setWrap(argument[i+1]);
            i++;
        } else if(argument[i] == "-n"){
            speechBubble.setDontWrap();
        } else if(argument[i] == "-J"){
            speechBubble.setAlignment(argument[i+1]);
            i++;
        }
    }
    speechBubble.drawSpeechBubble();
    cow.drawCow();
    return 0;
}