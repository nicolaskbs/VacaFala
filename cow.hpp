#ifndef COW_HPP_
#define COW_HPP_

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

enum CowType{
    DEFAULT,
    BORG,
    DEAD,
    GREEDY,
    PARANOIA,
    STONED,
    TIRED,
    WIRED,
    YOUNG
};

//imprime a vaca
class Cow{
private:
    CowType cowType = CowType::DEFAULT;
    std::string eyes;
    std::string tongue;
    bool userDefinedEyes = false;
    bool userDefinedTongue = false;
    bool userDefinedFlip = false;
    std::vector<std::string> cowLine;
public:
    Cow(CowType cowType);
    void setCow();
    void setTongue();
    void setEyes();
    void setType(CowType cowType);
    void userEyes(std::string eyes);
    void userTongue(std::string tongue);
    void drawCow();
    void flip();
    void setFlip();
    int findBiggest();
};

#endif
