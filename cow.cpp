#include "cow.hpp"

void Cow::setCow(){
    if(! this->userDefinedEyes){
        this->setEyes();
    }
    if(! this->userDefinedTongue){
        this->setTongue();
    }
    this->cowLine.push_back("        \\   ^__^");
    this->cowLine.push_back("         \\  (" + this->eyes + ")\\_______");
    this->cowLine.push_back("            (__)\\       )\\/\\");
    this->cowLine.push_back("              " + this->tongue + "||----w |");
    this->cowLine.push_back("                ||     ||");
    if(this->userDefinedFlip){
        this->flip();
    }
}

void Cow::setEyes(){
    switch(this->cowType){
        case CowType::DEFAULT:
            this->eyes = "oo";
            break;
        case CowType::BORG:
            this->eyes = "==";
            break;
        case CowType::DEAD:
            this->eyes = "xx";
            break;
        case CowType::GREEDY:
            this->eyes = "$$";
            break;
        case CowType::PARANOIA:
            this->eyes = "@@";
            break;
        case CowType::STONED:
            this->eyes = "**";
            break;
        case CowType::TIRED:
            this->eyes = "--";
            break;
        case CowType::WIRED:
            this->eyes = "OO";
            break;
        case CowType::YOUNG:
            this->eyes = "..";
            break;
    }
}

void Cow::setTongue(){
    if(this->cowType == DEAD || this->cowType == STONED){
        this->tongue = "U ";
    } else{
        this->tongue = "  ";
    }
}

Cow::Cow(CowType cowType){
    this->cowType = cowType;
}

void Cow::setType(CowType cowType){
    this->cowType = cowType;
}

void Cow::userEyes(std::string eyes){
    this->userDefinedEyes = true;
    this->eyes = eyes[0];
    if(eyes.size() == 1){
        this->eyes += eyes[0];
    } else{
        this->eyes += eyes[1];
    }
}

void Cow::userTongue(std::string tongue){
    this->userDefinedTongue = true;
    this->tongue = tongue[0];
    if(tongue.size() == 1){
        this->tongue += " ";
    } else{
        this->tongue += tongue[1];
    }
}

void Cow::drawCow(){
    this->setCow();
    for(int i = 0; i < this->cowLine.size(); i++){
        std::cout << this->cowLine[i] << std::endl;
    }
}

void Cow::flip(){
    for(int i = 0; i < this->cowLine.size(); i++){
        for(int j = 0; j < this->cowLine[i].size(); j++){
            if(this->cowLine[i][j] == '{'){
                this->cowLine[i][j] = '}';
            } else if(this->cowLine[i][j] == '}'){
                this->cowLine[i][j] = '{';
            } else if(this->cowLine[i][j] == '['){
                this->cowLine[i][j] = ']';
            } else if(this->cowLine[i][j] == ']'){
                this->cowLine[i][j] = '[';
            } else if(this->cowLine[i][j] == '('){
                this->cowLine[i][j] = ')';
            } else if(this->cowLine[i][j] == ')'){
                this->cowLine[i][j] = '(';
            } else if(this->cowLine[i][j] == '<'){
                this->cowLine[i][j] = '>';
            } else if(this->cowLine[i][j] == '\\'){
                this->cowLine[i][j] = '/';
            } else if(this->cowLine[i][j] == '/'){
                this->cowLine[i][j] = '\\';
            } else if(this->cowLine[i][j] == '>'){
                this->cowLine[i][j] = '<';
            }
        }
    }
    int biggest = findBiggest();
    for(int i = 0; i < this->cowLine.size(); i++){
        int aux = this->cowLine[i].size();
        for(int j = aux; j < biggest; j++){
            this->cowLine[i] += " ";
        }
        std::reverse(this->cowLine[i].begin(), this->cowLine[i].end());
    }
}

void Cow::setFlip(){
    this->userDefinedFlip = true;
}

int Cow::findBiggest(){
    int biggest = 0;
    for(int i = 0; i < this->cowLine.size(); i++){
        if(this->cowLine[i].size() > biggest){
            biggest = this->cowLine[i].size();
        }
    }
    return biggest;
}
