#include "speechbubble.hpp"

void SpeechBubble::getUserText(){
    std::string temporary;
    while(std::getline(std::cin, temporary)){
        this->phrase.push_back(temporary);
    }
    this->getWords();
}

void SpeechBubble::getWords() {
    std::vector<std::string> auxLine;
    std::string auxWord = "";
    bool wordBegan = false;
    for(int i = 0; i < this->phrase.size(); i++) {
        for(int j = 0; j < this->phrase[i].size(); j++) {
            if(this->phrase[i][j] != ' ' && this->phrase[i][j] != '\n' && this->phrase[i][j] != '\t') {
                auxWord += this->phrase[i][j];
                wordBegan = true;
            } else if(wordBegan) {
                auxLine.push_back(auxWord);
                auxWord = "";
                wordBegan = false;
            }
        }
        if(wordBegan) {
            auxLine.push_back(auxWord);
            auxWord = "";
            wordBegan = false;
        }
        this->word.push_back(auxLine);
        auxLine.clear();
    }
}

void SpeechBubble::setWrap(std::string ag){
    this->wrap = (std::stoi)(ag);
}

void SpeechBubble::setDontWrap(){
    this->dontWrap = true;
}

void SpeechBubble::setAlignment(std::string ch){
    this->alignment = ch;
}

void SpeechBubble::setSpeechBubble(){
    this->getUserText();
    int counter = 0;
    int previousCounter = 0;
    int lines = 0;
    if(dontWrap){
        this->wrap = findBiggest();
        for(int i = 0; i < this->phrase.size(); i++){
            this->bubbleLine.push_back(this->phrase[i]);
            if(this->bubbleLine[i].size() < this->wrap){
                for(int j = this->bubbleLine[i].size(); j < this->wrap; j++){
                    this->bubbleLine[i] += " ";
                }
            }
        }
    } else{
        if(this->alignment == "r"){

        } else if(this->alignment == "j"){

        } else{
        }
    }
}

void SpeechBubble::drawSpeechBubble(){
    setSpeechBubble();
    std::cout << " ";
    for(int i = 0; i < this->wrap; i++){
        std::cout << "_";
    }
    std::cout << std::endl;
    if(this->phrase.size() == 0){
        std::cout << "<";
        for(int i = 0; i < this->wrap + 2; i++){
            std::cout << " ";
        }
        std::cout << ">" << std::endl;
    } else if(this->bubbleLine.size() == 1){
        std::cout << "< " << this->bubbleLine[0] << " >" << std::endl;
    } else{
        std::cout << "/ " << this->bubbleLine[0] << " \\" << std::endl;
        for(int i = 1; i < this->bubbleLine.size() - 1; i++){
            std::cout << "| " << this->bubbleLine[i] << " |" << std::endl;
        }
        std::cout << "\\ " << this->bubbleLine[this->bubbleLine.size() - 1] << " /" << std::endl;
    }
    std::cout << " ";
    for(int i = 0; i < this->wrap; i++){
        std::cout << "-";
    }
    std::cout << std::endl;
}

int SpeechBubble::findBiggest(){
    int biggest = 0;
    for(int i = 0; i < this->phrase.size(); i++){
        if(this->phrase[i].size() > biggest){
            biggest = this->phrase[i].size();
        }
    }
    return biggest;
}

